import React, { Component } from 'react';
import { Container, Row, Button, Form } from 'reactstrap';
import ImgErrors from './ImgErrors.jsx';
import Word from './Word.jsx';
import Keyboard from './Keyboard.jsx';
import '../styles.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedoAlt } from '@fortawesome/free-solid-svg-icons';

class Guillotine extends Component {
    constructor(props) {
        super(props);

        let randomWord = this.getRandomWord();

        this.state = {
            errors: "7",
            hitNumber: "0",
            guessWord: randomWord,
            rightWords: this.hiddenWord(randomWord),
            buttons: this.getButtons(),
            succes: ''
        }

        this.getButtons = this.getButtons.bind(this);
        this.buttonClick = this.buttonClick.bind(this);
        this.getRandomWord = this.getRandomWord.bind(this);
        this.hitWord = this.hitWord.bind(this);
        this.hiddenWord = this.hiddenWord.bind(this);
        this.restart = this.restart.bind(this);

    }

    getRandomWord() {
        let words = [
            "INTERFAZ", "ACCESIBILIDAD", "USABILIDAD", "PROTOTIPO",
            "STORYBOARD", "TESTEO", "PERSONA", "PRODUCTO", "HIPERENLACE",
            "USUARIO", "ESCENARIO", "HIPERTEXTO", "CONTENIDO", "EXPERIENCIA",
            "MINIMALISTA", "IDEA", "PROCESOS", "INTERATIVIDAD", "DETALLES",
            "DISEÑO", "EQUIPO", "BOCETO", "ESTILO", "DESARROLLO", "TEXTURA",
            "EMOTICONOS", "DOCUMENTO", "EFECTO", "CREATIVIDAD", "SILUETA",
            "ILUSTRAR", "INTERATIVO", "RETROSPECTIVA", "ESTRATEGIA", "IDENTIDAD",
            "LOGO", "ANALIZAR", "IMPLEMENTAR", "MARCA", "FORMA", "TRAZO", "FIRMA",
            "CALIDAD", "RESOLUTIVIDAD"
        ];
        let randomNumber = Math.floor(Math.random() * words.length);
        let word = words[randomNumber];
        return word;
    }

    hiddenWord(word) {
        let guions = "";
        for (let i = 0; i < word.length; i++) {
            guions += "_";
        }
        return guions;
    }

    getButtons() {
        let letters = [
            "Q", "W", "E", "R", "T", "Y", "U", "I", "O",
            "P", "A", "S", "D", "F", "G", "H", "J", "K",
            "L", "Ñ", "Z", "X", "C", "V", "B", "N", "M"
        ]

        let buttons = letters.map((letter) => ({ letter, status: "not-clicked" }))

        return buttons;
    }

    buttonClick(p) {

        let letter = this.state.buttons[p].letter
        let auxButtons = this.state.buttons;
        let succes = ''
        let i = -1;
        while (i < this.state.guessWord.length) {
            i++
            if (this.state.guessWord[i] === letter) {
                succes = true
            }

        }

        if (succes) {
            this.hitWord(letter)

            auxButtons[p].status = "correct";
            this.setState((prevState) => ({
                hitNumber: ++(prevState.hitNumber),
                buttons: auxButtons
            }))
        }
        else {
            console.log('estamos en el else');

            auxButtons[p].status = "error";
            this.setState((prevState) => ({
                errors: --(prevState.errors),
                buttons: auxButtons
            })
            )
        }
    }

    hitWord(letter) {
        /*corregir*/
        for (let i = 0; i < this.state.guessWord.length; i++) {

            if (this.state.guessWord[i] === letter) {
                this.setState((prevState) => ({
                    numberHits: ++(prevState.numberHits),
                    rightWords: prevState.rightWords.substr(0, i) + letter +
                        prevState.rightWords.substr(i + 1)
                }));
            }
        }
    }
    restart() {

        let randomWord = this.getRandomWord();
        this.setState({
            errors: "7",
            hitNumber: "0",
            guessWord: randomWord,
            rightWords: this.hiddenWord(randomWord),
            buttons: this.getButtons(),
            succes: ''

        })
    }

    render() {
        let lose;
        let body;
        if (this.state.errors === 1) {
            lose =
                <>
                    <div className="box-img-errors">
                        <ImgErrors NumberErrors={this.state.errors} />
                    </div>
                    <div className="box-heart">
                        <img className="heart" src="images/heart.png" width="100px" alt="Guillotina" />
                    </div>
                    <div className="box-numberHeart">
                        <p className="errorsWord" >{this.state.errors - 1}</p>
                    </div>

                    <div style={{ position: "absolute", marginLeft: '400px' }}>
                        <div className="box-button-lose">
                        < FontAwesomeIcon onClick={this.restart} className="button-reload-lose" icon={faRedoAlt} />
                        </div>
                        <img width="1000px" src="images/lose.png" alt="lose" />
                    </div>
                </>;
        }
        else if (this.state.rightWords === this.state.guessWord) {
            body =
                <>
                    <img className="img-win" src="images/win.png" alt="" />
                    < FontAwesomeIcon onClick={this.restart} className="button-reload-win" icon={faRedoAlt} />
                </>
        } else {
            body = <>
                <div className="box-img-errors">
                    <ImgErrors NumberErrors={this.state.errors} />
                </div>
                <div className="box-heart">
                    <img className="heart" src="images/heart.png" width="100px" alt="Guillotina" />
                </div>
                <div className="box-numberHeart">
                    <p className="errorsWord" >{this.state.errors - 1}</p>
                </div>
                <div className="box-words">
                    <Word Words={this.state.rightWords} />
                </div>
                <div className="box-keyboard">
                    <Keyboard buttonClick={(i) => this.buttonClick(i)} buttons={this.state.buttons} />
                </div>
            </>
        }

        return (
            <>
                {this.state.errors === 1 ? lose : body}
            </>

        )
    }
}

export default Guillotine;