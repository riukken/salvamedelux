import React, { Component } from 'react';
import { Button } from 'reactstrap';
import '../styles.css';

class Keyboard extends Component {
    constructor(props) {
        super(props);
        this.getKeyboard = this.getKeyboard.bind(this);
    }
    getKeyboard() {
        return this.props.buttons.map((button, index) => (
            <button
                className={button.status}
                key={index}
                disabled={button.status !== "not-clicked" ? true : false }
                onClick={()=>this.props.buttonClick(index)}>
                {button.letter}
            </button>
        ))
    }
    render() {
        return (
            <>
                {this.getKeyboard()}
            </>

        )
    }
}

export default Keyboard;
