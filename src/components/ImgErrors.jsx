import React, { Component } from 'react';
import { Container, Row, Button, Form } from 'reactstrap';


class ImgErrors extends Component {
    constructor(props) {
        super(props);
        this.getImage = this.getImage.bind(this);
    }

    getImage() {
        return "images/" + this.props.NumberErrors + ".png"
    }
    // src={require("../images/0.jpg")}

    render() {
        return (
            <>
                <img width="1100px" src={this.getImage()}alt="Imagen ahorcado" />
            </>
        );
    }
}
export default ImgErrors;